==USER==

{
	"_userId": String,
	"password": String,
	"email": String
}

==AUCTION==
{
	"_auctionId": String,
	"code": String,
	"name": String,
	"listDate": Date
}

==PORTFOLIO==
{
	"_recordId": String,
	"auctionId": String,
	"quantity": Number,
	"userId": String,
	"purchasePrice": Number
}

==TRADE==
{
	"_tradeId": String,
	"price": Number,
	"date": Date,
	"auctionId": String,
	"buyerId": String,
	"quantity": Number,
	"sellerId": String
}

==BID==
{
	"_bidId": String,
	"type": String,
	"price":  Number,
	"quantity": Number,
	"userId": String,
	"auctionId": String,
	"submitDate": Date,
	"expiryDate": Date,
	"status": String
}